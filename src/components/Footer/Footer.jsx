import React from "react";
import footerImg from "../../images/logozim.png";

import styles from "./Footer.module.css";
import { Typography } from "@material-ui/core";

const Footer = () => {
    return (
        <div className={styles.container}>
            <a href="http://alexandrebrito.com"><img src={footerImg} className={styles.imgSet} /></a>
            <Typography>Feito por alexandrebrito.com</Typography>
        </div>
    )
}

export default Footer;