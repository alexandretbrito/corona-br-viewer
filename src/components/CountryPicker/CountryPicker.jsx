import React, { useState, useEffect } from "react";
import { NativeSelect, FormControl, Typography } from '@material-ui/core';
import { fetchStates } from '../../api';

import styles from './CountryPicker.module.css';

const CountryPicker = ( { handlePlaceChenge }) => {
    const [fetchedStates, setfetchedStates] = useState([]);

    useEffect(() => {
        const fetchAPI = async () =>{
            setfetchedStates(await fetchStates())
        }
        fetchAPI();
    }, [setfetchedStates]);

    const handleSub = (sigla) => {
        var StatesName = fetchedStates.filter( nation =>  nation.sigla === sigla );
        if(StatesName != ""){
            handlePlaceChenge(sigla, StatesName[0].nome);
        }else{
            handlePlaceChenge(sigla, "");
        }

   }

    return (
        <div>
            <Typography variant="h5" align="center"  color="textPrimary" gutterBottom>Escolha o estado para visualizar os dados.</Typography>
            <FormControl className={styles.formControl}>
                <NativeSelect defaultValue="" onChange={(e) => {handleSub(e.target.value)}}>
                    <option value="">Panorama Nacional</option>
                    {fetchedStates.map((state, i) => <option key ={i} value={state.sigla}>{state.nome}</option>)}
                </NativeSelect>
            </FormControl>
        </div>
    )
}

export default CountryPicker;