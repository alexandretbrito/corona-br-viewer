import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

const theme = createMuiTheme({
    typography: {
        fontFamily: "Cuprum, sans-serif",
        fontSize: 14,
        fontWeightBold: 700,
        fontWeightMedium: 700,
        fontWeightRegular: 400,
    }
});
ReactDOM.render(
<MuiThemeProvider theme= {theme}>
<App />
</MuiThemeProvider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
