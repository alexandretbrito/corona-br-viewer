import axios from 'axios';

const covidUrl = "https://covid19.mathdro.id/api";
/* retorno de dados ao vivo e estados */
const covidBrUrl = "https://covid19-brazil-api.now.sh/api/report/v1/brazil";
/* retorno de dados day one /dayly */
const covidBrUrlDayly= "https://api.covid19api.com/total/country/brazil";
/*
export const fetchData = async (place) => {
    let changeableUrl = covidBrUrl;
    if(place){
        changeableUrl = covidBrUrl+'/uf/'+place; 
    }
    try {
        const {data: { confirmed, recovered, deaths, lastUpdate }} = await axios.get(changeableUrl);

        return { confirmed, recovered, deaths, lastUpdate };
    } catch (error) {
        console.log("esse estado não existe");
        console.log(error);
    }

};
*/
export const fetchBrData = async (place) => {
    
    let changeableUrl = covidBrUrl;
    if(place){
        console.log("vem de um estado")
        changeableUrl = covidBrUrl+'/uf/'+place; 
    }
    
    try {
        const {data} = await axios.get(changeableUrl);
        let dataSet;
        if (place) {
            dataSet = {
                confirmed: data.cases, 
                recovered: data.recovered,
                deaths: data.deaths, 
                lastUpdate: data.datetime
            };            
        } else {
            dataSet = {
                confirmed: data.data.confirmed, 
                recovered: data.data.recovered,
                deaths: data.data.deaths, 
                lastUpdate: data.data.updated_at
            };            
        }
        return { dataSet };

    } catch (error) {
        console.log("esse estado não existe");
        console.log(error);
    }

};



export const fetchDataDaily = async () => {
    try {
        const { data } = await axios.get(covidBrUrlDayly);
        
        const modifiedData = data.map((dailyData) => ({
            confirmed: dailyData.Confirmed,
            deaths: dailyData.Deaths,
            recovered: dailyData.Recovered,
            date: new Date(dailyData.Date).toLocaleDateString('pt-BR'),
        }))
        return modifiedData;
    } catch (error) {
        console.log(error);
    }
};

export const fetchStates = async () => {
    try {
        const { data: {UF} } = await axios.get('./data/json-estados-brasileiros.json');

        return UF.map((state) => ({
                nome: state.nome,
                sigla: state.sigla
        }));
        console.log(UF);
    } catch (error) {
        console.log(error);
    }
};