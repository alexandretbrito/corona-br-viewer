import React from 'react';
import {Cards, Chart, CountryPicker, Footer} from './components/index';
import {fetchBrData} from "./api";
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions   from '@material-ui/core/DialogActions';
import {Typography} from "@material-ui/core";
import logo from "./images/corona-logo-br.png";

import styles from './App.module.css';

class App extends React.Component {
  state= {
    data: {},
    dataBr: {},
    place: '',
    nome: 'Panorama Nacional',
    the_open: false,
  };

  async componentDidMount() {
    const fetchedBrData = await fetchBrData();
    console.log(fetchedBrData);
    this.setState({dataBr: {
      confirmed: fetchedBrData.dataSet.confirmed, 
      recovered: fetchedBrData.dataSet.recovered, 
      deaths: fetchedBrData.dataSet.deaths, 
      lastUpdate: fetchedBrData.dataSet.lastUpdate
    }});
    console.log(this.state.dataBr);
  }

  handlePlaceChenge = async (place, nome) => {
    const fetchedData = await fetchBrData(place);
    console.log("retorno");
    console.log(fetchedData);
    console.log(nome)
    if (fetchedData) {
      if(nome === ""){
        nome = "Panorama Nacional";
      };
      let range = {
        confirmed: fetchedData.dataSet.confirmed, 
        recovered: fetchedData.dataSet.recovered, 
        deaths: fetchedData.dataSet.deaths, 
        lastUpdate: fetchedData.dataSet.lastUpdate
      }
      this.setState({dataBr: range, place: place, nome: nome});
      //this.setState({dataBr: fetchedData, place: place, nome: nome});
      console.log(this.state.dataBr);
      console.log(this.state);
    } else { 
      this.handleClickOpen();
    }
  };

  handleClickOpen = () => {
    this.setState({the_open: true})
  };

  handleClose = () => {
    this.setState({the_open: false})
  };


  render(){
    const { data, dataBr, place, the_open, nome } = this.state;

    return (
      <div className={styles.container}>
        <img src={logo} className={styles.logo}/>
        <Cards data={dataBr} name={nome} />
        <CountryPicker handlePlaceChenge={this.handlePlaceChenge} />
        <Chart data={dataBr} place={place} />
        <Footer />
        
        <Dialog onClose={this.handleClose} aria-labelledby="customized-dialog-title" open={the_open}>
        <DialogTitle id="customized-dialog-title" onClose={this.handleClose}>
          Informações não encontrados
        </DialogTitle>
        <DialogContent>
          <Typography gutterBottom>
           As informações relativas ao estado escolhido não constam no banco de dados. Por favor, tente um outro.
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleClose}>
            OK
          </Button>
        </DialogActions>
      </Dialog>
      </div>

    );
  }
} 

export default App;